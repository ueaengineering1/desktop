﻿namespace StudyPlanner.Models
{
    public enum TaskType
    {
        LAB,
        LECTURE,
        SEMINAR,
        COURSEWORK,
        MILESTONE,
        ACTIVITY
    }
}
