﻿namespace StudyPlanner.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public class Task : AbstractModel, IXmlSerializable
    {
        private static int idIncrement = 0;

        private int id;
        private string name;
        private DateTime startDate;
        private DateTime endDate;
        private bool completion;
        private string description;
        private TaskType type;
        private List<Note> notes;
        private List<int> dependencies;

        private Task() { }

        private Task(int id, string name, DateTime startDate, DateTime endDate, string description, TaskType type)
        {
            this.id = id;
            this.name = name;
            this.startDate = startDate;
            this.endDate = endDate;
            this.description = description;
            this.type = type;
            this.completion = false;
            this.notes = new List<Note>();
            this.dependencies = new List<int>();
        }

        public static Task CreateTask(String name, DateTime startDate, DateTime endDate, String description, TaskType type)
        {
            return new Task(idIncrement++, name, startDate, endDate, description, type);
        }

        public void AddNote(string name, string text)
        {
            this.notes.Add(Note.CreateNote(name, text));
            this.NotifyPropertyChanged("notes");
        }

        public List<Note> GetAllNotes()
        {
            return this.notes;
        }

        public Note GetNote(int index)
        {
            foreach (Note n in this.notes)
            {
                if (n.GetId() == index)
                {
                    return n;
                }
            }

            throw new KeyNotFoundException("Not a valid note id for this task!");
        }

        public void RemoveNote(int index)
        {
            this.notes.Remove(this.GetNote(index));
            this.NotifyPropertyChanged("notes");
        }

        public void SetAsComplete()
        {
            this.completion = true;
            this.NotifyPropertyChanged("completion");
        }

        public void SetAsIncomplete()
        {
            this.completion = false;
            this.NotifyPropertyChanged("completion");
        }

        public List<int> GetDependencies()
        {
            return this.dependencies;
        }

        public void AddDependency(int index)
        {
            this.dependencies.Add(index);
            this.NotifyPropertyChanged("dependency");
        }

        public void RemoveDependency(int index)
        {
            this.dependencies.Remove(index);
            this.NotifyPropertyChanged("dependency");
        }

        public int GetId()
        {
            return this.id;
        }

        public DateTime GetStartDate()
        {
            return this.startDate;
        }

        public DateTime GetEndDate()
        {
            return this.endDate;
        }

        public string GetTaskType()
        {
            return Enum.GetName(typeof(TaskType), this.type);
        }

        public void SetEndDate(DateTime input)
        {
            if (input >= this.startDate)
            {
                this.endDate = input;
                this.NotifyPropertyChanged("endDate");
            }
            else
            {
                throw new DataMisalignedException("End date can't be before the start date!");
            }
        }

        public void SetStartDate(DateTime input)
        {
            if (input <= this.endDate)
            {
                this.startDate = input;
                this.NotifyPropertyChanged("startDate");
            }
            else
            {
                throw new DataMisalignedException("Start date cant be after the end date!");
            }
        }

        public bool GetCompletion()
        {
            return this.completion;
        }

        public string GetName()
        {
            return this.name;
        }

        public TaskType GetTaskTypeObj()
        {
            return this.type;
        }

        public string GetDescription()
        {
            return this.description;
        }

        public void SetDescription(string description)
        {
            this.description = description;
            this.NotifyPropertyChanged("description");
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public void SetType(TaskType type)
        {
            this.type = type;
        }

        public void Listner(object sender, EventArgs e)
        {
            this.NotifyPropertyChanged("task");
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            this.name = reader.GetAttribute("name");
            this.startDate = DateTime.Parse(reader.GetAttribute("startDate"));
            this.endDate = DateTime.Parse(reader.GetAttribute("endDate"));
            this.description = reader.GetAttribute("description");
            this.completion = bool.Parse(reader.GetAttribute("completion"));
            this.type = (TaskType) Enum.Parse(typeof(TaskType), reader.GetAttribute("type"));
            this.id = int.Parse(reader.GetAttribute("id"));
            idIncrement = int.Parse(reader.GetAttribute("idIncrement"));
            this.notes = new List<Note>();
            this.dependencies = new List<int>();

            string depen = reader.GetAttribute("dependencies");

            if (!string.IsNullOrEmpty(depen)){
                List<string> listdepen = depen.Split(';').ToList();

                foreach (string i in listdepen) {
                    this.dependencies.Add(int.Parse(i));
                }
            }

            if (reader.ReadToDescendant("TaskNotes"))
            {
                if (reader.ReadToDescendant("note"))
                {
                    do
                    {
                        var constructor = typeof(Note).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], null);
                        var note = (Note)constructor.Invoke(null);

                        note.ReadXml(reader);
                        this.notes.Add(note);

                    } while (reader.ReadToNextSibling("note"));
                    reader.ReadEndElement();
                }
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("idIncrement", idIncrement.ToString());
            writer.WriteAttributeString("id", this.id.ToString());
            writer.WriteAttributeString("name", this.name);
            writer.WriteAttributeString("completion", this.completion.ToString());
            writer.WriteAttributeString("description", this.description);
            writer.WriteAttributeString("type", this.type.ToString());
            writer.WriteAttributeString("startDate", this.startDate.ToString());
            writer.WriteAttributeString("endDate", this.endDate.ToString());
            writer.WriteAttributeString("dependencies", string.Join(";", this.dependencies.ToArray()));

            if (this.notes.Any())
            {
                writer.WriteStartElement("TaskNotes");
            }

            foreach (Note n in this.notes)
            {
                writer.WriteStartElement("note");
                n.WriteXml(writer);
                writer.WriteEndElement();
            }

            if (this.notes.Any())
            {
                writer.WriteEndElement();
            }
        }
    }
}
