﻿namespace StudyPlanner.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public class User : AbstractModel, IXmlSerializable
    {
        private static int idIncrement = 0;

        private int id;
        private string name;
        private string school;
        private string course;
        private List<Module> modules;

        private User() { }

        private User(int id, string name, string school, string course)
        {
            this.id = id;
            this.name = name;
            this.school = school;
            this.course = course;
            this.modules = new List<Module>();
        }

        public static User CreateUser(string name, string school, string course)
        {
            return new User(idIncrement++, name, school, course);

        }

        public int GetId()
        {
            return this.id;
        }

        public string GetName()
        {
            return this.name;
        }

        public void SetName(string name)
        {
            this.name = name;
            this.NotifyPropertyChanged("name");
        }

        public string GetSchool()
        {
            return this.school;
        }

        public string GetCourse()
        {
            return this.course;
        }

        public void AddModule(string name, DateTime startDate, DateTime endDate, string moduleCode, string moduleOrganiser, float courseworkAmount, bool exam)
        {
            Module tempM = Module.CreateModule(name, startDate, endDate, moduleCode, moduleOrganiser, courseworkAmount, exam);
            this.modules.Add(tempM);
            tempM.PropertyChanged += this.Listner;
            this.NotifyPropertyChanged("modules");
        }

        public void AddModule(Module m)
        {
            this.modules.Add(m);
            m.PropertyChanged += this.Listner;
            this.NotifyPropertyChanged("modules");
        }

        public List<Module> GetAllModules()
        {
            return this.modules;
        }

        public Module GetModule(int index)
        {
            foreach (Module m in this.modules)
            {
                if (m.GetId() == index)
                {
                    return m;
                }
            }

            throw new KeyNotFoundException("Not a valid module id for this user!");
        }

        public void RemoveModule(int index)
        {
            this.modules.Remove(this.GetModule(index));
            this.NotifyPropertyChanged("modules");
        }

        public void Listner(object sender, EventArgs e)
        {
            this.NotifyPropertyChanged("user");
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadToDescendant("user");
            idIncrement = int.Parse(reader.GetAttribute("idIncrement"));
            this.id = int.Parse(reader.GetAttribute("id"));
            this.name = reader.GetAttribute("name");
            this.school = reader.GetAttribute("school");
            this.course = reader.GetAttribute("course");
            this.modules = new List<Module>();

            if (reader.ReadToDescendant("UserModules"))
            {
                if (reader.ReadToDescendant("module"))
                {
                    do
                    {
                        var constructor = typeof(Module).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], null);
                        var instance = (Module)constructor.Invoke(null);

                        instance.ReadXml(reader);
                        instance.PropertyChanged += this.Listner;
                        this.modules.Add(instance);

                    } while (reader.ReadToNextSibling("module"));
                }
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("user");

            writer.WriteAttributeString("idIncrement", idIncrement.ToString());
            writer.WriteAttributeString("id", this.id.ToString());
            writer.WriteAttributeString("name", this.name);
            writer.WriteAttributeString("school", this.school);
            writer.WriteAttributeString("course", this.course);

            if (this.modules.Any())
            {
                writer.WriteStartElement("UserModules");
            }

            foreach (Module m in this.modules)
            {
                writer.WriteStartElement("module");
                m.WriteXml(writer);
                writer.WriteEndElement();
            }

            if (this.modules.Any())
            {
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }
    }
}
