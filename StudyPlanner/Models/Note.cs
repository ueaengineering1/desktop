﻿namespace StudyPlanner.Models
{
    using System;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public class Note : IXmlSerializable
    {
        private static int idIncrement = 0;

        private int id;
        private string name;
        private string text;

        private Note() { }

        private Note(int id, string name, string text)
        {
            this.id = id;
            this.name = name;
            this.text = text;
        }

        public static Note CreateNote(string name, string text)
        {
            return new Note(idIncrement++, name, text);
        }

        public int GetId()
        {
            return this.id;
        }

        public string GetText()
        {
            return this.text;
        }

        public string GetName()
        {
            return this.name;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            this.id = int.Parse(reader.GetAttribute("id"));
            this.name = reader.GetAttribute("name");
            this.text = reader.GetAttribute("text");
            idIncrement = int.Parse(reader.GetAttribute("idIncrement"));
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("idIncrement", idIncrement.ToString());
            writer.WriteAttributeString("id", this.id.ToString());
            writer.WriteAttributeString("name", this.name);
            writer.WriteAttributeString("text", this.text);
        }
    }
}
