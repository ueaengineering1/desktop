﻿namespace StudyPlanner.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public class Module : AbstractModel, IXmlSerializable
    {
        private static int idIncrement = 0;

        private int id;
        private string name;
        private DateTime startDate;
        private DateTime endDate;
        private bool exam;
        private float courseworkAmount;
        private string moduleCode;
        private string moduleOrganiser;
        private List<Task> tasks;
        private List<Note> notes;

        private Module() { }

        private Module(int id, string name, DateTime startDate, DateTime endDate, string moduleCode, string moduleOrganiser, float courseworkAmount, bool exam)
        {
            this.id = id;
            this.name = name;
            this.startDate = startDate;
            this.endDate = endDate;
            this.moduleCode = moduleCode;
            this.moduleOrganiser = moduleOrganiser;
            this.courseworkAmount = courseworkAmount;
            this.exam = exam;
            this.tasks = new List<Task>();
            this.notes = new List<Note>();
        }

        public static Module CreateModule(string name, DateTime startDate, DateTime endDate, string moduleCode, string moduleOrganiser, float courseworkAmount, bool exam)
        {
            return new Module(idIncrement++, name, startDate, endDate, moduleCode, moduleOrganiser, courseworkAmount, exam);
        }

        public bool GetExam()
        {
            return this.exam;
        }

        public string GetModuleOrganiser()
        {
            return this.moduleOrganiser;
        }

        public float GetCourseworkAmount()
        {
            return this.courseworkAmount;
        }

        public string GetModuleCode()
        {
            return this.moduleCode;
        }

        public int GetId()
        {
            return this.id;
        }

        public DateTime GetStartDate()
        {
            return this.startDate;
        }

        public DateTime GetEndDate()
        {
            return this.endDate;
        }

        public void SetEndDate(DateTime input)
        {
            //if (input > this.startDate)
            //{
                this.endDate = input;
                this.NotifyPropertyChanged("endDate");
            //}
            //else
            //{
            //    throw new DataMisalignedException("End date can't be before the start date!");
            //}
        }

        public void SetStartDate(DateTime input)
        {
            //if (input < this.endDate) {
                this.startDate = input;
                this.NotifyPropertyChanged("startDate");
            //}
            //else
            //{
            //    throw new DataMisalignedException("Start date cant be after the end date!");
            //}
        }

        public int AddTask(string name, DateTime startDate, DateTime endDate, string description, TaskType type)
        {
            Task tempT = Task.CreateTask(name, startDate, endDate, description, type);
            this.tasks.Add(tempT);
            tempT.PropertyChanged += this.Listner;
            this.NotifyPropertyChanged("tasks");
            return tempT.GetId();
        }

        public List<Task> GetAllTasks()
        {
            return this.tasks;
        }

        public Task GetTask(int index)
        {
            foreach (Task t in this.tasks)
            {
                if (t.GetId() == index)
                {
                    return t;
                }
            }

            throw new KeyNotFoundException("Not a valid task id for this module!");
        }

        public void RemoveTask(int index)
        {
            this.tasks.Remove(this.GetTask(index));
            this.NotifyPropertyChanged("tasks");
        }

        public string GetName()
        {
            return this.name;
        }

        public void AddNote(string name, string text)
        {
            this.notes.Add(Note.CreateNote(name, text));
            this.NotifyPropertyChanged("notes");
        }

        public List<Note> GetAllNotes()
        {
            return this.notes;
        }

        public Note GetNote(int index)
        {
            foreach (Note n in this.notes)
            {
                if (n.GetId() == index)
                {
                    return n;
                }
            }

            throw new KeyNotFoundException("Not a valid note id for this module!");
        }

        public void RemoveNote(int index)
        {
            this.notes.Remove(this.GetNote(index));
            this.NotifyPropertyChanged("notes");
        }

        public void Listner(object sender, EventArgs e)
        {
            this.NotifyPropertyChanged("module");
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("idIncrement", idIncrement.ToString());
            writer.WriteAttributeString("id", this.id.ToString());
            writer.WriteAttributeString("name", this.name);
            writer.WriteAttributeString("startDate", this.startDate.ToString());
            writer.WriteAttributeString("endDate", this.endDate.ToString());
            writer.WriteAttributeString("exam", this.exam.ToString());
            writer.WriteAttributeString("courseworkAmount", this.courseworkAmount.ToString());
            writer.WriteAttributeString("moduleCode", this.moduleCode);
            writer.WriteAttributeString("moduleOrganiser", this.moduleOrganiser);

            if (this.notes.Any())
            {
                writer.WriteStartElement("ModuleNotes");
            }

            foreach (Note n in this.notes)
            {
                writer.WriteStartElement("note");
                n.WriteXml(writer);
                writer.WriteEndElement();
            }

            if (this.notes.Any())
            {
                writer.WriteEndElement();
            }

            if (this.tasks.Any())
            {
                writer.WriteStartElement("ModuleTasks");
            }

            foreach (Task t in this.tasks)
            {
                writer.WriteStartElement("task");
                t.WriteXml(writer);
                writer.WriteEndElement();
            }

            if (this.tasks.Any())
            {
                writer.WriteEndElement();
            }
        }

        public void ReadXml(XmlReader reader)
        {
            this.name = reader.GetAttribute("name");
            this.startDate = DateTime.Parse(reader.GetAttribute("startDate"));
            this.endDate = DateTime.Parse(reader.GetAttribute("endDate"));
            this.moduleCode = reader.GetAttribute("moduleCode");
            this.moduleOrganiser = reader.GetAttribute("moduleOrganiser");
            this.courseworkAmount = float.Parse(reader.GetAttribute("courseworkAmount"));
            this.exam = bool.Parse(reader.GetAttribute("exam"));
            this.id = int.Parse(reader.GetAttribute("id"));
            idIncrement = int.Parse(reader.GetAttribute("idIncrement"));
            this.notes = new List<Note>();
            this.tasks = new List<Task>();

            if (reader.ReadToDescendant("ModuleNotes"))
            {
                if (reader.ReadToDescendant("note"))
                {
                    do
                    {
                        var constructor = typeof(Note).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], null);
                        var note = (Note)constructor.Invoke(null);

                        note.ReadXml(reader);
                        this.notes.Add(note);

                    } while (reader.ReadToNextSibling("note"));
                }
            }

            if (reader.ReadToFollowing("ModuleTasks"))
            {
                if (reader.ReadToDescendant("task"))
                {
                    do
                    {
                        var constructor = typeof(Task).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], null);
                        var task = (Task)constructor.Invoke(null);

                        task.ReadXml(reader);
                        task.PropertyChanged += this.Listner;
                        this.tasks.Add(task);

                    } while (reader.ReadToNextSibling("task"));
                }
            }

            reader.ReadEndElement();
        }

        public XmlSchema GetSchema()
        {
            return null;
        }
    }
}
