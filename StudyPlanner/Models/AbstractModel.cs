﻿namespace StudyPlanner.Models
{
    using System.ComponentModel;
    using StudyPlanner.Helpers;

    public abstract class AbstractModel : INotifyPropertyChanged
    {

        protected AbstractModel()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                UserSerializer.WriteUserToXML();
        }
    }
}
