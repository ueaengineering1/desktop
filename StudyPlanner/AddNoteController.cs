﻿using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlanner.Controllers.NoteControllers
{
    class AddNoteController
    {
        //If only a module id and note string are passed then this will add a note to the module with the id
        public static void AddNote(int moduleID, String note)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.getModule(moduleID);
            module.addNote(note);
        }

        //If a module id and task id are passed with the note the task of that module will have a note added
        public static void AddNote(int moduleID, int taskID, String note)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.getModule(moduleID);

            List<Models.Task> tasks = module.getAllTasks();
            tasks[taskID].addNote(note);
        }


    }
}
