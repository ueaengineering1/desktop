﻿namespace StudyPlanner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        private ObservableCollection<TempTask> tsk;
        private ObservableCollection<TempTask> dls;
        private ObservableCollection<TempNote> nts;
        private ObservableCollection<TempTask> cmpTask;
        private bool showingComp = false;
        private Controllers.ModuleControllers.ReturnModuleController.ReturnModule selectedMod;
        private int selectedModuleID;

        public Dashboard()
        {
            this.InitializeComponent();
        }

        private void RefreshNotes()
        {
            this.selectedMod = Controllers.ModuleControllers.ReturnModuleController.ViewModule(this.selectedModuleID);
            this.LB_ModNotes.ItemsSource = null;
            List<Controllers.ModuleControllers.ReturnModuleController.ReturnNote> notes = this.selectedMod.ModuleNotes;
            this.nts = new ObservableCollection<TempNote>();
            foreach (var n in notes)
            {
                this.nts.Add(new TempNote(n.NoteName, n.NoteId));
            }

            this.LB_ModNotes.ItemsSource = this.nts;
        }

        private void RefreshTasks()
        {
            this.BTN_Completed.Content = "Completed";
            this.PGB_ModuleProgress.Value = Controllers.TaskControllers.PercentageCompletionController.ReturnPercentage(this.selectedModuleID);
            this.showingComp = false;
            this.selectedMod = Controllers.ModuleControllers.ReturnModuleController.ViewModule(this.selectedModuleID);
            this.LB_Task.ItemsSource = null;
            List<Controllers.ModuleControllers.ReturnModuleController.ReturnTask> tasks = this.selectedMod.ModuleTasks;
            this.tsk = new ObservableCollection<TempTask>();
            this.cmpTask = new ObservableCollection<TempTask>();
            foreach (var t in tasks)
            {
                Controllers.TaskControllers.ReturnTaskController.ReturnTask task = Controllers.TaskControllers.ReturnTaskController.ReturnTaskData(this.selectedModuleID, t.TaskId);
                if (task.Completion == false)
                {
                    this.tsk.Add(new TempTask(task.TaskName, task.Type, task.TaskID, task.Completion));
                }
                else
                {
                    this.cmpTask.Add(new TempTask(task.TaskName, task.Type, task.TaskID, task.Completion));
                }
            }

            this.LB_Task.ItemsSource = this.tsk;

            this.LB_Deadlines.ItemsSource = null;
            List<Controllers.TaskControllers.ModuleDeadlinesController.ReturnModuleDeadlines> deadlines = Controllers.TaskControllers.ModuleDeadlinesController.ReturnDeadlines(this.selectedModuleID);
            this.dls = new ObservableCollection<TempTask>();
            foreach (var d in deadlines)
            {
                this.dls.Add(new TempTask(d.TaskTitle, d.TaskID, d.EndDate));
            }

            this.LB_Deadlines.ItemsSource = this.dls;
        }

        // Select a Module
        private void LB_Modules_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Need to populate notes / tasks list boxes.
            // Need to do progress bar.
            if (this.LB_Modules.SelectedIndex != -1)
            {
                // selectedModuleID = Int32.Parse(e.AddedItems[0].ToString().Split('-')[0]);
                ListBox lbx = (ListBox)sender;
                this.selectedModuleID = Int32.Parse(lbx.SelectedValue.ToString());
                this.selectedMod = Controllers.ModuleControllers.ReturnModuleController.ViewModule(this.selectedModuleID);

                this.RefreshTasks();
                this.RefreshNotes();

                this.BTN_CreateTask.IsEnabled = true;
                this.BTN_CreateModNote.IsEnabled = true;
                this.BTN_ViewGChart.IsEnabled = true;

                this.BTN_Completed.IsEnabled = true;
                this.BTN_Completed.Content = "Completed";
                this.showingComp = false;

                // tasks = Controllers.TaskControllers.ModuleTaskController.returnAllModuleTasks(selectedModuleID);
                // LB_Task.ItemsSource = tasks;
            }
            else
            {
                this.selectedModuleID = -1;
                this.LB_Task.ItemsSource = null;
                this.LB_ModNotes.ItemsSource = null;
                this.LB_Deadlines.ItemsSource = null;

                this.BTN_CreateTask.IsEnabled = false;
                this.BTN_CreateModNote.IsEnabled = false;
                this.BTN_ViewGChart.IsEnabled = false;
                this.BTN_Completed.IsEnabled = false;
                this.BTN_Completed.Content = "Completed";
                this.showingComp = false;
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Load data from controllers.
            List<Controllers.ModuleControllers.ReturnModulesController.ReturnModule> mods = Controllers.ModuleControllers.ReturnModulesController.ReturnAllModules(0);
            ObservableCollection<TempModule> mds = new ObservableCollection<TempModule>();
            this.LB_Modules.ItemsSource = mds;
            foreach (var m in mods)
            {
                mds.Add(new TempModule(m.ModuleName, m.ModuleID));
            }

            Controllers.DashboardControllers.ReturnDashboard.ReturnDashboardInfo inf = Controllers.DashboardControllers.ReturnDashboard.returnDashboardInfo();
            this.LAB_Name.Content = "Name: " + inf.Name;
            this.LAB_School.Content = "School: " + inf.School;
            this.LAB_Course.Content = "Course: " + inf.Course;
        }

        private void BTN_DeselectAll_Click(object sender, RoutedEventArgs e)
        {
            this.LB_Modules.SelectedIndex = -1;
            this.LB_Task.SelectedIndex = -1;
            this.LB_ModNotes.SelectedIndex = -1;
            this.LB_Deadlines.SelectedIndex = -1;
            this.PGB_ModuleProgress.Value = 0;
        }

        private void LB_Task_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.LB_Task.SelectedIndex != -1)
            {
                this.PGB_ModuleProgress.Value = Controllers.TaskControllers.PercentageCompletionController.ReturnPercentage(this.selectedModuleID);

                // int selectedTaskID = Int32.Parse(e.AddedItems[0].ToString().Split('-')[0]);
                ListBox lbx = (ListBox)sender;
                int selectedTaskID = Int32.Parse(lbx.SelectedValue.ToString());
                Views.Task.Task t = new Views.Task.Task(Controllers.TaskControllers.ReturnTaskController.ReturnTaskData(this.selectedModuleID, selectedTaskID), false);
                t.ShowDialog();
                this.RefreshTasks();
            }
        }

        private void LB_ModNotes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.LB_ModNotes.SelectedIndex != -1)
            {
                ListBox lbx = (ListBox)sender;
                int selectedNoteID = Int32.Parse(lbx.SelectedValue.ToString());
                List<Controllers.NoteControllers.ModuleNoteController.ReturnNotes> selectedModNotes = Controllers.NoteControllers.ModuleNoteController.ReturnAllModuleNotes(selectedModuleID);
                foreach (var note in selectedModNotes)
                {
                    if (note.NoteID == selectedNoteID)
                    {
                        Views.Note.Note n = new Views.Note.Note(note, false);
                        n.ShowDialog();
                        this.RefreshNotes();
                    }
                }
            }
        }

        private void LB_Deadlines_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.LB_Deadlines.SelectedIndex != -1)
            {
                this.PGB_ModuleProgress.Value = Controllers.TaskControllers.PercentageCompletionController.ReturnPercentage(this.selectedModuleID);
                ListBox lbx = (ListBox)sender;
                int selectedTaskID = Int32.Parse(lbx.SelectedValue.ToString());
                Views.Task.Task t = new Views.Task.Task(Controllers.TaskControllers.ReturnTaskController.ReturnTaskData(this.selectedModuleID, selectedTaskID), false);
                t.ShowDialog();
                this.RefreshTasks();
            }
        }

        private void BTN_CreateTask_Click(object sender, RoutedEventArgs e)
        {
            Controllers.TaskControllers.ReturnTaskController.ReturnTask t = new Controllers.TaskControllers.ReturnTaskController.ReturnTask
            {
                ModuleID = this.selectedModuleID,
                TaskID = -1
            };
            Views.Task.Task ts = new Views.Task.Task(t, true);
            ts.ShowDialog();
            this.RefreshTasks();
        }

        private void BTN_CreateModNote_Click(object sender, RoutedEventArgs e)
        {
            Controllers.NoteControllers.ModuleNoteController.ReturnNotes n = new Controllers.NoteControllers.ModuleNoteController.ReturnNotes
            {
                ModuleID = this.selectedModuleID,
                NoteID = -1
            };

            Views.Note.Note nt = new Views.Note.Note(n, true);
            nt.ShowDialog();

            // LB_ModNotes.ItemsSource = null;
            // LB_ModNotes.ItemsSource = nts;
            this.RefreshNotes();
        }

        private void BTN_Completed_Click(object sender, RoutedEventArgs e)
        {
            if (this.showingComp == false)
            {
                this.LB_Task.ItemsSource = null;
                this.LB_Task.ItemsSource = this.cmpTask;
                this.BTN_Completed.Content = "To Do";
                this.showingComp = true;
            }
            else
            {
                this.LB_Task.ItemsSource = null;
                this.LB_Task.ItemsSource = this.tsk;
                this.BTN_Completed.Content = "Completed";
                this.showingComp = false;
            }
        }

        private void BTN_ViewHelp_Click(object sender, RoutedEventArgs e)
        {
            Views.Help.Help h = new Views.Help.Help();
            h.ShowDialog();
        }

        private void BTN_ViewGChart_Click(object sender, RoutedEventArgs e)
        {
            Views.GanttChart.GanttChart g = new Views.GanttChart.GanttChart(this.selectedModuleID);
            g.Show();
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.F1)
            {
                Views.Help.Help h = new Views.Help.Help();
                h.ShowDialog();
            }
        }

        public class TempTask
        {
            public TempTask(string title, string type, int id, bool completion)
            {
                this.title = title;
                this.type = type;
                this.id = id;
                this.completion = completion;
                this.due = DateTime.MinValue;
            }

            public TempTask(string title, int id, DateTime due)
            {
                this.title = title;
                this.type = null;
                this.id = id;
                this.completion = false;
                this.due = due;
            }

            public string title { get; set; }

            public string type { get; set; }

            public int id { get; set; }

            public bool completion { get; set; }

            public DateTime due { get; set; }

            public override string ToString()
            {
                return this.title + " - " + this.due.ToShortDateString();
            }
        }

        public class TempNote
        {
            public TempNote(string title, int id)
            {
                this.title = title;
                this.id = id;
            }

            public string title { get; set; }

            public int id { get; set; }
        }

        public class TempModule
        {
            public TempModule(string title, int id)
            {
                this.title = title;
                this.id = id;
            }

            public string title { get; set; }

            public int id { get; set; }
        }
    }
}
