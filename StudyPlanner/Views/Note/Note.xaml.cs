﻿namespace StudyPlanner.Views.Note
{
    using System;
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for Note.xaml
    /// </summary>
    public partial class Note : Window
    {
        private Controllers.NoteControllers.ModuleNoteController.ReturnNotes note;
        private Controllers.NoteControllers.TaskNoteController.ReturnNotes tNote;
        private bool creatingNew = true;
        private bool isModNote;
        private int taskID;
        private int noteID;

        public Note(Controllers.NoteControllers.ModuleNoteController.ReturnNotes note, bool creatingNew)
        {
            this.InitializeComponent();

            this.note = note;
            this.isModNote = true;
            this.creatingNew = creatingNew;

            // Set task id to -1 if no task.
            this.taskID = -1;
            this.noteID = note.NoteID;

            if (!creatingNew)
            {
                this.TB_Title.Text = note.NoteName;
                this.TB_Content.Text = note.NoteText;
                this.BTN_Delete.IsEnabled = true;
            }
            else
            {
                this.BTN_Save.Content = "Create";
            }
        }

        public Note(Controllers.NoteControllers.TaskNoteController.ReturnNotes note, bool creatingNew, int taskID)
        {
            this.InitializeComponent();
            this.isModNote = false;
            this.tNote = note;
            this.creatingNew = creatingNew;
            this.taskID = taskID;
            this.noteID = this.tNote.NoteID;
            if (!creatingNew)
            {
                this.TB_Title.Text = this.tNote.NoteName;
                this.TB_Content.Text = this.tNote.NoteText;
                this.BTN_Delete.IsEnabled = true;
            }
        }

        private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
        {
            // this.DialogResult = true;
            this.Close();
        }

        private void BTN_Save_Click(object sender, RoutedEventArgs e)
        {
            if(String.IsNullOrWhiteSpace(TB_Title.Text))
            {
                MessageBox.Show("Please enter a Note title.");
            }
            else
            {
                if (this.creatingNew)
                {
                    // Create a new note.
                    if (this.taskID == -1)
                    {
                        Controllers.NoteControllers.AddNoteController.AddModuleNote(this.note.ModuleID, this.TB_Title.Text, this.TB_Content.Text);
                    }
                    else
                    {
                        Controllers.NoteControllers.AddNoteController.AddTaskNote(this.tNote.ModuleID, this.taskID, this.TB_Title.Text, this.TB_Content.Text);
                    }
                }
                else
                {
                    if (this.taskID == -1)
                    {
                        // delete first
                        Controllers.NoteControllers.DeleteNoteController.DeleteModuleNote(this.note.ModuleID, this.note.NoteID);
                        Controllers.NoteControllers.AddNoteController.AddModuleNote(this.note.ModuleID, this.TB_Title.Text, this.TB_Content.Text);
                    }
                    else
                    {
                        // delete first
                        Controllers.NoteControllers.DeleteNoteController.DeleteTaskNote(this.tNote.ModuleID, this.tNote.TaskID, this.tNote.NoteID);
                        Controllers.NoteControllers.AddNoteController.AddTaskNote(this.tNote.ModuleID, this.taskID, this.TB_Title.Text, this.TB_Content.Text);
                    }
                }

                this.DialogResult = true;
                this.Close();
            }
        }

        private void BTN_Delete_Click(object sender, RoutedEventArgs e)
        {
            if (this.taskID == -1)
            {
                Controllers.NoteControllers.DeleteNoteController.DeleteModuleNote(this.note.ModuleID, this.note.NoteID);
            }
            else
            {
                Controllers.NoteControllers.DeleteNoteController.DeleteTaskNote(this.tNote.ModuleID, this.tNote.TaskID, this.tNote.NoteID);
            }

            this.DialogResult = true;
            this.Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                Help.Help h = new Help.Help();
                h.ShowDialog();
            }
        }
    }
}
