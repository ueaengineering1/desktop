﻿namespace StudyPlanner.Views.Task
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for Task.xaml
    /// </summary>
    public partial class Task : Window
    {
        private static List<string> types = new List<string>()
        {
            "LECTURE", "LAB", "SEMINAR", "COURSEWORK", "MILESTONE", "ACTIVITY"
        };

        private Controllers.TaskControllers.ReturnTaskController.ReturnTask task;
        private bool creatingNew;
        private ObservableCollection<TempNote> nts;
        private List<int> depsInts;
        private ObservableCollection<TempTask> dps;
        private int parentTaskID = -1;

        public Task(Controllers.TaskControllers.ReturnTaskController.ReturnTask tsk, bool creatingNew)
        {
            this.InitializeComponent();
            this.task = tsk;
            this.creatingNew = creatingNew;
            this.CB_TaskType.ItemsSource = types;

            if (!creatingNew)
            {
                this.TB_Title.Text = this.task.TaskName;
                this.DP_SetDate.SelectedDate = this.task.StartDate;
                this.DP_DueDate.SelectedDate = this.task.EndDate;
                this.TB_Desc.Text = this.task.Description;
                this.CB_TaskType.SelectedItem = this.task.Type;
                this.CH_Completed.IsChecked = this.task.Completion;
                this.BTN_Delete.IsEnabled = true;
                this.RefreshNotes();
                this.RefreshDependencies();
            }
            else
            {
                this.DP_SetDate.SelectedDate = DateTime.Now;
                this.DP_DueDate.SelectedDate = DateTime.Now;
                this.CB_TaskType.SelectedItem = "ACTIVITY";
                this.LB_TNotes.IsEnabled = false;
                this.BTN_CreateNote.IsEnabled = false;
                this.BTN_CreateDep.IsEnabled = false;
                this.LB_Dependencies.IsEnabled = false;
                this.BTN_UpdateTask.Content = "Create";
            }
        }

        public Task(Controllers.TaskControllers.ReturnTaskController.ReturnTask tsk, bool creatingNew, int parentTaskID)
        {
            this.InitializeComponent();
            this.task = tsk;
            this.creatingNew = creatingNew;
            this.CB_TaskType.ItemsSource = types;

            if (!creatingNew)
            {
                this.TB_Title.Text = this.task.TaskName;
                this.DP_SetDate.SelectedDate = this.task.StartDate;
                this.DP_DueDate.SelectedDate = this.task.EndDate;
                this.TB_Desc.Text = this.task.Description;
                this.CB_TaskType.SelectedItem = this.task.Type;
                this.CH_Completed.IsChecked = this.task.Completion;
                this.BTN_Delete.IsEnabled = true;
                this.RefreshNotes();
                this.RefreshDependencies();
            }
            else
            {
                this.DP_SetDate.SelectedDate = DateTime.Now;
                this.DP_DueDate.SelectedDate = DateTime.Now;
                this.CB_TaskType.SelectedItem = "ACTIVITY";
                this.LB_TNotes.IsEnabled = false;
                this.BTN_CreateNote.IsEnabled = false;
                this.BTN_CreateDep.IsEnabled = false;
                this.LB_Dependencies.IsEnabled = false;
                this.BTN_UpdateTask.Content = "Create";
            }

            this.parentTaskID = parentTaskID;
        }

        private void RefreshNotes()
        {
            this.task = Controllers.TaskControllers.ReturnTaskController.ReturnTaskData(this.task.ModuleID, this.task.TaskID);
            List<Controllers.TaskControllers.ReturnTaskController.ReturnNote> notes = this.task.Notes;
            this.LB_TNotes.ItemsSource = null;
            this.nts = new ObservableCollection<TempNote>();
            foreach (var n in notes)
            {
                this.nts.Add(new TempNote(n.NoteName, n.NoteId));
            }

            this.LB_TNotes.ItemsSource = this.nts;
        }

        private void RefreshDependencies()
        {
            this.task = Controllers.TaskControllers.ReturnTaskController.ReturnTaskData(this.task.ModuleID, this.task.TaskID);
            this.depsInts = this.task.Dependencies;
            this.dps = new ObservableCollection<TempTask>();
            foreach (int i in this.depsInts)
            {
                Controllers.TaskControllers.ReturnTaskController.ReturnTask dep = Controllers.TaskControllers.ReturnTaskController.ReturnTaskData(this.task.ModuleID, i);
                this.dps.Add(new TempTask(dep.TaskName, i, dep.EndDate, dep.Completion));
            }

            this.LB_Dependencies.ItemsSource = this.dps;
        }

        private void BTN_CreateNote_Click(object sender, RoutedEventArgs e)
        {
            Controllers.NoteControllers.TaskNoteController.ReturnNotes n = new Controllers.NoteControllers.TaskNoteController.ReturnNotes
            {
                ModuleID = this.task.ModuleID,
                NoteID = -1
            };

            Note.Note nt = new Note.Note(n, true, this.task.TaskID);
            nt.ShowDialog();
            nt = null;
            this.RefreshNotes();
        }

        private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BTN_Delete_Click(object sender, RoutedEventArgs e)
        {
            if (!this.creatingNew)
            {
                Controllers.TaskControllers.DeleteTaskController.DeleteTask(this.task.ModuleID, this.task.TaskID);
                this.Close();
            }
        }

        private void BTN_UpdateTask_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(TB_Title.Text))
            {
                MessageBox.Show("Please enter a task title.");
            }
            else
            {
                if (this.creatingNew)
                {
                    if (this.parentTaskID == -1)
                    {
                        if (DP_DueDate.SelectedDate >= DP_SetDate.SelectedDate)
                        {
                            Controllers.ModuleControllers.AddTaskController.AddTask(this.task.ModuleID, this.TB_Title.Text, (DateTime)this.DP_SetDate.SelectedDate, (DateTime)this.DP_DueDate.SelectedDate, this.TB_Desc.Text, this.CB_TaskType.Text);
                        }
                        else
                        {
                            MessageBox.Show("Due date cannot be before Set date!");
                        }
                    }
                    else
                    {
                        // We are creating a subtask and so we need to add to the parents list of dependencies.
                        if (DP_DueDate.SelectedDate >= DP_SetDate.SelectedDate)
                        {
                            Controllers.TaskControllers.ReturnTaskController.ReturnTask parentTask = Controllers.TaskControllers.ReturnTaskController.ReturnTaskData(task.ModuleID, parentTaskID);

                            if ((DP_SetDate.SelectedDate >= parentTask.StartDate) && (DP_DueDate.SelectedDate <= parentTask.EndDate))
                            {
                                int newID = Controllers.ModuleControllers.AddTaskController.AddTask(this.task.ModuleID, this.TB_Title.Text, (DateTime)this.DP_SetDate.SelectedDate, (DateTime)this.DP_DueDate.SelectedDate, this.TB_Desc.Text, this.CB_TaskType.Text);
                                Controllers.TaskControllers.AddDependency.AddTaskDependency(this.task.ModuleID, this.parentTaskID, newID);
                            }
                            else
                            {
                                MessageBox.Show("Dependency start/end dates must be within the parent tasks start/end dates.");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Due date cannot be before Set date!");
                        }
                    }

                    this.Close();
                }
                else
                {
                    if (DP_DueDate.SelectedDate >= DP_SetDate.SelectedDate)
                    {
                        Controllers.TaskControllers.UpdateTaskController.UpdateTask t = new Controllers.TaskControllers.UpdateTaskController.UpdateTask
                        {
                            Name = this.TB_Title.Text,

                            StartDate = (DateTime)this.DP_SetDate.SelectedDate,
                            EndDate = (DateTime)this.DP_DueDate.SelectedDate,
                            Type = this.CB_TaskType.Text,
                            Completion = (bool)this.CH_Completed.IsChecked,
                            Description = this.TB_Desc.Text,
                            ModuleId = this.task.ModuleID,
                            TaskId = this.task.TaskID
                        };
                        Controllers.TaskControllers.UpdateTaskController.UpdateSelectedTask(t);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Due date cannot be before Set date!");
                    }
                }
            }
        }

        private void LB_TNotes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.LB_TNotes.SelectedIndex != -1)
            {
                ListBox lbx = (ListBox)sender;
                int selectedNoteID = Int32.Parse(lbx.SelectedValue.ToString());
                List<Controllers.NoteControllers.TaskNoteController.ReturnNotes> selectedNotes = Controllers.NoteControllers.TaskNoteController.ReturnAllTaskNotes(this.task.TaskID, this.task.ModuleID);
                foreach (var note in selectedNotes)
                {
                    if (note.NoteID == selectedNoteID)
                    {
                        Views.Note.Note n = new Views.Note.Note(note, false, this.task.TaskID);
                        n.ShowDialog();
                        this.RefreshNotes();
                    }
                }
            }

            this.LB_TNotes.SelectedIndex = -1;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.F1)
            {
                Views.Help.Help h = new Views.Help.Help();
                h.ShowDialog();
            }
        }

        private void LB_Dependencies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.LB_Dependencies.SelectedIndex != -1)
            {
                ListBox lbx = (ListBox)sender;
                int selectedTaskID = Int32.Parse(lbx.SelectedValue.ToString());
                Task t = new Task(Controllers.TaskControllers.ReturnTaskController.ReturnTaskData(this.task.ModuleID, selectedTaskID), false);
                t.ShowDialog();
                this.RefreshDependencies();
            }
        }

        private void BTN_CreateDep_Click(object sender, RoutedEventArgs e)
        {
            Controllers.TaskControllers.ReturnTaskController.ReturnTask t = new Controllers.TaskControllers.ReturnTaskController.ReturnTask
            {
                ModuleID = this.task.ModuleID,
                TaskID = -1
            };
            Task ts = new Task(t, true, this.task.TaskID);
            ts.ShowDialog();
            this.RefreshDependencies();
        }

        public class TempTask
        {
            public TempTask(string title, string type, int id, bool completion)
            {
                this.title = title;
                this.type = type;
                this.id = id;
                this.completion = completion;
                this.due = DateTime.MinValue;
            }

            public TempTask(string title, int id, DateTime due, bool completion)
            {
                this.title = title;
                this.type = null;
                this.id = id;
                this.completion = completion;
                this.due = due;
            }

            public string title { get; set; }

            public string type { get; set; }

            public int id { get; set; }

            public bool completion { get; set; }

            public DateTime due { get; set; }

            public override string ToString()
            {
                return this.title + " - " + this.due;
            }
        }

        public class TempNote
        {
            public TempNote(string title, int id)
            {
                this.title = title;
                this.id = id;
            }

            public string title { get; set; }

            public int id { get; set; }
        }
    }
}
