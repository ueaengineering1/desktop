﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace StudyPlanner.Views.CreateUser
{
    /// <summary>
    /// Interaction logic for createUser.xaml
    /// </summary>
    public partial class createUser : Window
    {
        public createUser()
        {
                InitializeComponent();
        }

        private void BTN_selectFile_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            string filePath = "";

            OpenFileDialog ofd1 = new OpenFileDialog();
            ofd1.Filter = "Semester import xml (*.xml)|*.xml|All files (*.*)|*.*";
            ofd1.FilterIndex = 2;
            ofd1.DefaultExt = ".xml";
            ofd1.RestoreDirectory = true;

            bool? result = ofd1.ShowDialog();

            if (result == true)
            {
                TB_file.Text = ofd1.FileName;
                BTN_createUser.IsEnabled = true;
                //TB_file.Text = filePath;
                filePath = ofd1.FileName;
            }
        }

        private void BTN_createUser_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(TB_name.Text) || String.IsNullOrWhiteSpace(TB_school.Text) || String.IsNullOrWhiteSpace(TB_course.Text))
            {
                MessageBox.Show("Please make sure to enter your details correctly.");
            }
            else
            {
                BTN_createUser.IsEnabled = false;

                string name = TB_name.Text;
                string school = TB_school.Text;
                string course = TB_course.Text;
                string file = TB_file.Text;
                String result = Controllers.UserControllers.CreateUserController.CreateGlobalUser(name, school, course, file);
                if (result == "User created successfully!")
                {
                    //close create user
                    MessageBox.Show(result);
                    Dashboard db = new Dashboard();
                    db.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error creating user, check file.");
                    BTN_createUser.IsEnabled = true;

                    TB_name.Text = "";
                    TB_school.Text = "";
                    TB_course.Text = "";
                    TB_file.Text = "";
                }
            }
            
        }

        private void BTN_createUser_Loaded(object sender, RoutedEventArgs e)
        {
            //Try load here, if file present, then load dashboard. Else do nothing.
            try
            {
                Helpers.UserSerializer.ReadUserFromXML();
                Dashboard db = new Dashboard();
                db.Show();
                this.Close();
            }
            catch (Exception)
            {
                InitializeComponent();
            }
        }

        private void BTN_Help_Click(object sender, RoutedEventArgs e)
        {
            Views.Help.Help h = new Help.Help();
            h.ShowDialog();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.F1)
            {
                Views.Help.Help h = new Views.Help.Help();
                h.ShowDialog();
            }
        }
    }
}
