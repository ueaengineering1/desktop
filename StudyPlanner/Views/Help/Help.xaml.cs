﻿namespace StudyPlanner.Views.Help
{
    using System.Collections.ObjectModel;
    using System.Windows;

    /// <summary>
    /// Interaction logic for Help.xaml
    /// </summary>
    public partial class Help : Window
    {
        public Help()
        {
            this.InitializeComponent();
            ObservableCollection<string> colours = new ObservableCollection<string>
            {
                "LECTURE",
                "LAB",
                "SEMINAR",
                "COURSEWORK",
                "MILESTONE",
                "ACTIVITY"
            };
            this.LB_Colours.ItemsSource = colours;

            ObservableCollection<string> status = new ObservableCollection<string>
            {
                "Completed",
                "To Do"
            };
            this.LB_Status.ItemsSource = status;
        }
    }
}
