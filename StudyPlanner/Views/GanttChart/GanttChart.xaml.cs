﻿namespace StudyPlanner.Views.GanttChart
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows;
    using Braincase.GanttChart;
    using StudyPlanner.Controllers.GanttControllers;

    /// <summary>
    /// Interaction logic for GanttChart.xaml
    /// </summary>
    public partial class GanttChart : Window
    {
        private List<GanttTaskController.GanttTasks> tasks;
        private GanttModuleController.GanttModule module;
        private List<Dependency> dependencies;

        public GanttChart(int moduleId)
        {
            this.InitializeComponent();
            this.tasks = GanttTaskController.ReturnAllModuleTasks(moduleId);
            this.module = GanttModuleController.ReturnModule(moduleId);
            this.dependencies = new List<Dependency>();

            // Check each task to see if they have dependencies
            foreach (GanttTaskController.GanttTasks task in this.tasks)
            {
                // If a task has dependencies then create a new Dependency struct for each dependancy
                // and add them to the list to be used to create relations for the gantt chart
                if (task.Dependencies != null)
                {
                    foreach (int dep in task.Dependencies)
                    {
                        Dependency dependency = new Dependency
                        {
                            TaskID = dep,
                            ParentName = task.TaskName
                        };
                        this.dependencies.Add(dependency);
                    }
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.Integration.WindowsFormsHost host =
                new System.Windows.Forms.Integration.WindowsFormsHost();

            var manager = new ProjectManager
            {
                // Set the start of the gantt chart to the start of the module
                Start = this.module.ModuleStartDate
            };
            foreach (GanttTaskController.GanttTasks task in this.tasks)
            {
                // Create a new task for the gantt chart using the task name from the list
                var t = new Task() { Name = task.TaskName };

                // Add the task to the chart
                manager.Add(t);

                // Calculate and set the start of the task relative to the start of the module
                manager.SetStart(t, task.StartDate - this.module.ModuleStartDate);

                // Calculate and set the end of the task relative to the start of the module
                manager.SetEnd(t, task.EndDate - this.module.ModuleStartDate);

                // Calculate and set the duration of a task
                manager.SetDuration(t, TimeSpan.FromDays((task.EndDate - task.StartDate).TotalDays));

                // Check dependency list
                if (this.dependencies != null)
                {
                    // Iterate over all dependencies
                    foreach (Dependency dep in this.dependencies)
                    {
                        // Check if the current task is in the list
                        if (dep.TaskID == task.TaskID)
                        {
                            Task parent = null;

                            // If the task is in the list then find its parent task in the gantt tasks
                            foreach (Task tk in manager.Tasks)
                            {
                                // Get the parent of the task
                                if (tk.Name == dep.ParentName)
                                {
                                    parent = tk;
                                }
                            }

                            // Set the dependency for the task within the gantt chart
                            if (parent != null)
                            {
                                manager.Group(parent, t);
                            }
                        }
                    }
                }
            }

            var chart = new Chart();
            chart.Init(manager);

            // Iterate over every task and colour it based on its type
            chart.PaintTask += (s, ee) =>
            {
                if (ee.Task != null)
                {
                    var format = default(TaskFormat);
                    format = ee.Format;
                    foreach (GanttTaskController.GanttTasks task in this.tasks)
                    {
                        if (task.TaskName == ee.Task.Name)
                        {
                            Models.TaskType type = task.Type;

                            // Set the backfill color based on the task type
                            switch (type)
                            {
                                case Models.TaskType.ACTIVITY:
                                    format.BackFill = new SolidBrush(Color.Gainsboro);
                                    break;
                                case Models.TaskType.COURSEWORK:
                                    format.BackFill = new SolidBrush(Color.LightSalmon);
                                    break;
                                case Models.TaskType.LAB:
                                    format.BackFill = new SolidBrush(Color.LightBlue);
                                    break;
                                case Models.TaskType.LECTURE:
                                    format.BackFill = new SolidBrush(Color.Plum);
                                    break;
                                case Models.TaskType.MILESTONE:
                                    format.BackFill = new SolidBrush(Color.MediumSpringGreen);
                                    break;
                                case Models.TaskType.SEMINAR:
                                    format.BackFill = new SolidBrush(Color.LightPink);
                                    break;
                            }
                        }
                    }

                    // Set the color using the format option from the switch statement
                    ee.Format = format;
                }
            };

            // Disable moving of tasks in the gantt chart
            chart.AllowTaskDragDrop = false;
            host.Child = chart;
            this.grid1.Children.Add(host);

            // Set the view of the gantt to the current date
            chart.ScrollTo(DateTime.Now);
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.F1)
            {
                Help.Help h = new Help.Help();
                h.ShowDialog();
            }
        }

        public struct Dependency
        {
            public int TaskID;
            public string ParentName;
        }
    }
}

// https://github.com/jakesee/ganttchart