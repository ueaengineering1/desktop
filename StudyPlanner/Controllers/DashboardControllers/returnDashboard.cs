﻿namespace StudyPlanner.Controllers.DashboardControllers
{
    public class ReturnDashboard
    {
        public static ReturnDashboardInfo returnDashboardInfo()
        {
            Models.User user = Helpers.UserPersistance.GetCurrentUser();
            ReturnDashboardInfo r = new ReturnDashboardInfo
            {
                Name = user.GetName(),
                School = user.GetSchool(),
                Course = user.GetCourse()
            };
            return r;
        }

        public struct ReturnDashboardInfo
        {
            public string Name;
            public string School;
            public string Course;
        }
    }
}
