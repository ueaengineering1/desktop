﻿namespace StudyPlanner.Controllers.GanttControllers

{
    using System;
    using StudyPlanner.Models;

    public static class GanttModuleController
    {
        // Returns a ReturnModule struct for the gantt chart to use
        public static GanttModule ReturnModule(int moduleID)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);

            GanttModule rm = new GanttModule
            {
                ModuleID = module.GetId(),
                ModuleStartDate = module.GetStartDate()
            };
            return rm;
        }

        // Struct for the gantt charts to be created
        public struct GanttModule
        {
            public int ModuleID;
            public DateTime ModuleStartDate;

        };
    }
}
