﻿namespace StudyPlanner.Controllers.GanttControllers

{
    using System;
    using System.Collections.Generic;
    using StudyPlanner.Models;

    public static class GanttTaskController
    {
        // Returns a list of ReturnTask structs for the gantt chart to use
        public static List<GanttTasks> ReturnAllModuleTasks(int moduleID)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);

            List<Models.Task> tasks = module.GetAllTasks();
            List<GanttTasks> reTasks = new List<GanttTasks>();

            foreach (Models.Task task in tasks)
            {
                GanttTasks rt = default(GanttTasks);
                if (task.GetCompletion() == false)
                {
                    rt.ModuleID = module.GetId();
                    rt.TaskID = task.GetId();
                    rt.TaskName = task.GetName();
                    rt.StartDate = task.GetStartDate();
                    rt.EndDate = task.GetEndDate();
                    rt.Type = task.GetTaskTypeObj();
                    rt.Dependencies = task.GetDependencies();
                    reTasks.Add(rt);
                }
            }

            return reTasks;
        }

        // Struct for the gantt charts to be created
        public struct GanttTasks
        {
            public int ModuleID;
            public int TaskID;
            public string TaskName;
            public DateTime StartDate;
            public DateTime EndDate;
            public TaskType Type;
            public List<int> Dependencies;
        }
    }
}
