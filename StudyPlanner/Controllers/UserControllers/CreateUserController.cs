﻿namespace StudyPlanner.Controllers.UserControllers
{
    using System;
    using StudyPlanner.Helpers;
    using StudyPlanner.Models;

    public class CreateUserController
    {
        public static string CreateGlobalUser(string name, string school, string course, string filepath)
        {
            try
            {
                User user = User.CreateUser(name, school, course);
                UserPersistance.SetUser(user);
                Helpers.ProfileExporter.ImportFromXML(filepath);
                return "User created successfully!";
            }
            catch (NullReferenceException)
            {
                return "Unable to create user!";
            }
        }
    }
}
