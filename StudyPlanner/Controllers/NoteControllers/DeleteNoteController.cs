﻿namespace StudyPlanner.Controllers.NoteControllers
{
    using StudyPlanner.Helpers;
    using StudyPlanner.Models;

    public static class DeleteNoteController
    {
        public static bool DeleteModuleNote(int moduleId, int noteId)
        {
            Note n = UserPersistance.GetCurrentUser().GetModule(moduleId).GetNote(noteId);

            if (n != null)
            {
                UserPersistance.GetCurrentUser().GetModule(moduleId).RemoveNote(noteId);

                return true;
            }

            return false;
        }

        public static bool DeleteTaskNote(int moduleId, int taskId, int noteId)
        {
            Note n = UserPersistance.GetCurrentUser().GetModule(moduleId).GetTask(taskId).GetNote(noteId);

            if (n != null)
            {
                UserPersistance.GetCurrentUser().GetModule(moduleId).GetTask(taskId).RemoveNote(noteId);

                return true;
            }

            return false;
        }
    }
}
