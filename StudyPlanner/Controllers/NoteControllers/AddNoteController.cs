﻿namespace StudyPlanner.Controllers.NoteControllers
{
    using System.Collections.Generic;
    using StudyPlanner.Models;

    public class AddNoteController
    {
        public static void AddModuleNote(int moduleID, string name, string note)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);
            module.AddNote(name, note);
        }

        public static void AddTaskNote(int moduleID, int taskID, string name, string note)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);

            List<Models.Task> tasks = module.GetAllTasks();
            foreach (var t in tasks)
            {
                if (t.GetId() == taskID)
                {
                    t.AddNote(name, note);
                }
            }
        }
    }
}
