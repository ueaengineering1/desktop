﻿namespace StudyPlanner.Controllers.TaskControllers
{
    using System;
    using System.Collections.Generic;
    using StudyPlanner.Models;

    public class ModuleDeadlinesController
    {
        public static List<ReturnModuleDeadlines> ReturnDeadlines(int moduleID)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);

            List<Models.Task> tasks = module.GetAllTasks();
            List<ReturnModuleDeadlines> deadlines = new List<ReturnModuleDeadlines>();

            DateTime d = DateTime.Now.AddDays(14);

            foreach (Models.Task task in tasks)
            {
                ReturnModuleDeadlines deadline = default(ReturnModuleDeadlines);
                int dateCompare = DateTime.Compare(task.GetEndDate(), d);
                if (task.GetTaskTypeObj() == TaskType.COURSEWORK || task.GetTaskTypeObj() == TaskType.MILESTONE)
                {
                    if (task.GetCompletion() == false)
                    {
                        if (dateCompare < 0 && task.GetEndDate() > DateTime.Now)
                        {
                            deadline.ModuleID = moduleID;
                            deadline.TaskID = task.GetId();
                            deadline.EndDate = task.GetEndDate();
                            deadline.TaskTitle = task.GetName();

                            deadlines.Add(deadline);
                        }
                    }
                }
            }

            return deadlines;
        }

        public struct ReturnModuleDeadlines
        {
            public int ModuleID;
            public int TaskID;
            public string TaskTitle;
            public DateTime EndDate;

            public override string ToString()
            {
                return this.TaskTitle + " - " + this.EndDate.ToShortDateString();
            }
        }
    }
}
