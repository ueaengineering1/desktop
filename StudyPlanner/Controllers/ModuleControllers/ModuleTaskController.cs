﻿namespace StudyPlanner.Controllers.TaskControllers
{
    using System.Collections.Generic;
    using StudyPlanner.Models;

    public static class ModuleTaskController
    {
        public static List<ReturnTasks> ReturnAllModuleTasks(int moduleID)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);

            List<Models.Task> tasks = module.GetAllTasks();
            List<ReturnTasks> reTasks = new List<ReturnTasks>();

            foreach (Models.Task task in tasks)
            {
                ReturnTasks rt = new ReturnTasks
                {
                    ModuleID = module.GetId(),
                    TaskID = task.GetId(),
                    TaskName = task.GetName()
                };
                reTasks.Add(rt);
            }

            return reTasks;
        }

        public struct ReturnTasks
        {
            public int ModuleID;
            public string TaskName;
            public int TaskID;

            public override string ToString()
            {
                return this.TaskID + "-" + this.TaskName;
            }
        }
    }
}
