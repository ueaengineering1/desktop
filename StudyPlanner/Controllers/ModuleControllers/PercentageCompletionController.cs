﻿namespace StudyPlanner.Controllers.TaskControllers
{
    using System.Collections.Generic;
    using System.Linq;
    using StudyPlanner.Models;

    public class PercentageCompletionController
    {
        // return the persentage of task in a given module than have been completed 
        public static double ReturnPercentage(int moduleID)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);

            List<Models.Task> tasks = module.GetAllTasks();

            double count = tasks.Count();
            double done = 0;

            foreach (Models.Task task in tasks)
            {
                if (task.GetCompletion())
                {
                    done++;
                }
            }

            double percentage = (done / count) * 100;

            return percentage;
        }
    }
}
