namespace StudyPlanner.Controllers.ModuleControllers
{
    using System.Collections.Generic;

    public static class ReturnModulesController
    {
        public static List<ReturnModule> ReturnAllModules(int userID)
        {
            Models.User user = Helpers.UserPersistance.GetCurrentUser();

            List<Models.Module> modules = user.GetAllModules();
            List<ReturnModule> moduleNames =new List<ReturnModule>();

            foreach (Models.Module module in modules){
                ReturnModule rm = new ReturnModule
                {
                    ModuleID = module.GetId(),
                    ModuleName = module.GetName()
                };

                moduleNames.Add(rm);
            }

            return moduleNames;
        }

        public struct ReturnModule
        {
            public string ModuleName;
            public int ModuleID;

            public override string ToString()
            {
                return this.ModuleID + "-" + this.ModuleName;
            }
        }
    }
}
