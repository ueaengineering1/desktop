﻿namespace StudyPlanner.Controllers.NoteControllers
{
    using System.Collections.Generic;
    using StudyPlanner.Models;

    public static class ModuleNoteController
    {
        public static List<ReturnNotes> ReturnAllModuleNotes(int moduleID)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);

            List<Note> notes = module.GetAllNotes();
            List<ReturnNotes> reNotes = new List<ReturnNotes>();

            foreach (Note note in notes)
            {
                ReturnNotes rn = new ReturnNotes
                {
                    ModuleID = module.GetId(),
                    NoteName = note.GetName(),
                    NoteText = note.GetText(),
                    NoteID = note.GetId()
                };
                reNotes.Add(rn);
            }

            return reNotes;
        }

        public struct ReturnNotes
        {
            public int ModuleID;
            public string NoteName;
            public string NoteText;
            public int NoteID;
        }
    }
}
