﻿namespace StudyPlanner.Controllers.ModuleControllers
{
    using System;
    using StudyPlanner.Helpers;
    using StudyPlanner.Models;

    public static class AddTaskController
    {
        public static int AddTask(int moduleID, string name, DateTime startDate, DateTime endDate, string description, string type)
        {
            TaskType t = (TaskType) Enum.Parse(typeof(TaskType), type);
            return UserPersistance.GetCurrentUser().GetModule(moduleID).AddTask(name, startDate, endDate, description, t);
        }
    }
}
