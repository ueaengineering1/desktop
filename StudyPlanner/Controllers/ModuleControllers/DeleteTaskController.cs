﻿namespace StudyPlanner.Controllers.TaskControllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using StudyPlanner.Helpers;

    public static class DeleteTaskController
    {
        public static bool DeleteTask(int moduleId, int taskId)
        {
            Models.Task t = UserPersistance.GetCurrentUser().GetModule(moduleId).GetTask(taskId);

            if (t != null)
            {
                UserPersistance.GetCurrentUser().GetModule(moduleId).RemoveTask(taskId);
                return true;
            }

            return false;
        }
    }
}
