﻿namespace StudyPlanner.Controllers.ModuleControllers
{
    using System;
    using System.Collections.Generic;
    using StudyPlanner.Models;

    public static class ReturnModuleController
    {
        public static ReturnModule ViewModule(int moduleId)
        {
            Models.User user = Helpers.UserPersistance.GetCurrentUser();

            Module module = user.GetModule(moduleId);

            ReturnModule m = new ReturnModule();
            m.ModuleId = module.GetId();
            m.ModuleName = module.GetName();
            m.ModuleStartDate = module.GetStartDate();
            m.ModuleEndDate = module.GetEndDate();
            m.ModuleExam = module.GetExam();
            m.ModuleCourseworkAmount = module.GetCourseworkAmount();
            m.ModuleCode = module.GetModuleCode();
            m.ModuleOrganiser = module.GetModuleOrganiser();

            m.ModuleTasks = new List<ReturnTask>();
            m.ModuleNotes = new List<ReturnNote>();

            List<Models.Task> tasks = module.GetAllTasks();
            List<Note> notes = module.GetAllNotes();

            foreach (Models.Task task in tasks)
            {
                ReturnTask t = new ReturnTask
                {
                    TaskId = task.GetId(),
                    TaskName = task.GetName(),
                    Completion = task.GetCompletion()
                };
                m.ModuleTasks.Add(t);
            }

            foreach (Note note in notes)
            {
                ReturnNote n = new ReturnNote
                {
                    NoteId = note.GetId(),
                    NoteName = note.GetName(),
                    NoteText = note.GetText()
                };

                m.ModuleNotes.Add(n);
            }

            return m;
        }

        public struct ReturnModule
        {
            public int ModuleId;
            public string ModuleName;
            public DateTime ModuleStartDate;
            public DateTime ModuleEndDate;
            public bool ModuleExam;
            public float ModuleCourseworkAmount;
            public string ModuleCode;
            public string ModuleOrganiser;
            public List<ReturnTask> ModuleTasks;
            public List<ReturnNote> ModuleNotes;

            public override string ToString()
            {
                return this.ModuleId + "-" + this.ModuleName;
            }
        }

        public struct ReturnTask
        {
            public int TaskId;
            public string TaskName;
            public bool Completion;

            public override string ToString()
            {
                if (this.Completion)
                {
                    return this.TaskId + "-" + " \u2714 " + this.TaskName;
                }
                else
                {
                    return this.TaskId + "-" + " \u20DE " + this.TaskName;
                }
            }
        }

        public struct ReturnNote
        {
            public int NoteId;
            public string NoteName;
            public string NoteText;

            public override string ToString()
            {
                return this.NoteId + "-" + this.NoteName;
            }
        }
    }
}
