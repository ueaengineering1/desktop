﻿namespace StudyPlanner.Controllers.TaskControllers
{
    using StudyPlanner.Helpers;

    public static class AddDependency
    {
        public static void AddTaskDependency(int modID, int parent, int child)
        {
            UserPersistance.GetCurrentUser().GetModule(modID).GetTask(parent).AddDependency(child);
        }
    }
}
