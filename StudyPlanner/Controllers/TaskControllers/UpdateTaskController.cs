﻿namespace StudyPlanner.Controllers.TaskControllers
{
    using System;
    using StudyPlanner.Helpers;
    using StudyPlanner.Models;

    public static class UpdateTaskController
    {
        public static bool UpdateSelectedTask(UpdateTask update)
        {
            Models.Task t = UserPersistance.GetCurrentUser().GetModule(update.ModuleId).GetTask(update.TaskId);

            if (t != null) {
                TaskType type = (TaskType)Enum.Parse(typeof(TaskType), update.Type);
                UserPersistance.GetCurrentUser().GetModule(update.ModuleId).GetTask(update.TaskId).SetName(update.Name);
                UserPersistance.GetCurrentUser().GetModule(update.ModuleId).GetTask(update.TaskId).SetStartDate(update.StartDate);
                UserPersistance.GetCurrentUser().GetModule(update.ModuleId).GetTask(update.TaskId).SetEndDate(update.EndDate);
                UserPersistance.GetCurrentUser().GetModule(update.ModuleId).GetTask(update.TaskId).SetDescription(update.Description);
                UserPersistance.GetCurrentUser().GetModule(update.ModuleId).GetTask(update.TaskId).SetType(type);

                if (update.Completion)
                {
                    UserPersistance.GetCurrentUser().GetModule(update.ModuleId).GetTask(update.TaskId).SetAsComplete();
                }
                else
                {
                    UserPersistance.GetCurrentUser().GetModule(update.ModuleId).GetTask(update.TaskId).SetAsIncomplete();
                }

                return true;
            }

            return false;
        }

        public struct UpdateTask
        {
            public int ModuleId;
            public int TaskId;
            public string Name;
            public DateTime StartDate;
            public DateTime EndDate;
            public string Description;
            public string Type;
            public bool Completion;
        }
    }
}
