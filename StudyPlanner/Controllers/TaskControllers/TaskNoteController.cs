﻿namespace StudyPlanner.Controllers.NoteControllers
{
    using System.Collections.Generic;
    using StudyPlanner.Models;

    public static class TaskNoteController
    {
        public static List<ReturnNotes> ReturnAllTaskNotes(int taskID, int moduleID)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);
            Models.Task task = module.GetTask(taskID);

            List<Note> notes = task.GetAllNotes();
            List<ReturnNotes> reNotes = new List<ReturnNotes>();

            foreach (Note note in notes)
            {
                ReturnNotes rn = new ReturnNotes
                {
                    ModuleID = module.GetId(),
                    TaskID = task.GetId(),
                    NoteName = note.GetName(),
                    NoteText = note.GetText(),
                    NoteID = note.GetId()
                };
                reNotes.Add(rn);
            }

            return reNotes;
        }

        public struct ReturnNotes
        {
            public int ModuleID;
            public int TaskID;
            public string NoteName;
            public string NoteText;
            public int NoteID;
        }
    }
}
