﻿namespace StudyPlanner.Controllers.TaskControllers
{
    using System;
    using System.Collections.Generic;
    using StudyPlanner.Models;

    public class ReturnTaskController
    {
        public static ReturnTask ReturnTaskData(int moduleID, int taskID)
        {
            User user = Helpers.UserPersistance.GetCurrentUser();
            Module module = user.GetModule(moduleID);
            Models.Task task = module.GetTask(taskID);

            ReturnTask rt = new ReturnTask
            {
                ModuleID = module.GetId(),
                TaskID = task.GetId(),
                TaskName = task.GetName(),
                StartDate = task.GetStartDate(),
                EndDate = task.GetEndDate(),
                Completion = task.GetCompletion(),
                Description = task.GetDescription(),
                Type = task.GetTaskType(),
                Notes = new List<ReturnNote>()
            };
            List<Note> notes = task.GetAllNotes();
            rt.Dependencies = task.GetDependencies();

            foreach (Note note in notes)
            {
                ReturnNote n = new ReturnNote
                {
                    NoteId = note.GetId(),
                    NoteName = note.GetName()
                };
                rt.Notes.Add(n);
            }

            return rt;
        }

        public struct ReturnTask
        {
            public int ModuleID;
            public int TaskID;
            public string TaskName;
            public DateTime StartDate;
            public DateTime EndDate;
            public bool Completion;
            public string Description;
            public string Type;
            public List<ReturnNote> Notes;
            public List<int> Dependencies;
        }

        public struct ReturnNote
        {
            public int NoteId;
            public string NoteName;
        }
    }
}
