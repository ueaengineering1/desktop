﻿namespace StudyPlanner.Helpers
{
    using System;
    using System.IO;
    using StudyPlanner.Models;

    public class ProfileExporter
    {
        public static void ImportFromXML(string filePath)
        {
            if (UserPersistance.GetCurrentUser() != null)
            {
                System.Xml.Serialization.XmlSerializer reader =
                    new System.Xml.Serialization.XmlSerializer(typeof(Models.User));

                System.IO.StreamReader file = new System.IO.StreamReader(filePath);
                Models.User user = null;
                try
                {
                    user = (Models.User)reader.Deserialize(file);
                }
                catch (Exception)
                {
                    System.Windows.MessageBox.Show("File is not valid.");
                    var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//users_details.xml";
                    File.Delete(path);
                    System.Windows.Application.Current.Shutdown();
                }

                file.Close();
                foreach (Module m in user.GetAllModules())
                {
                    UserPersistance.GetCurrentUser().AddModule(m);
                }
            }
            else
            {
                throw new NullReferenceException("No user set globally!");
            }
        }
    }
}
