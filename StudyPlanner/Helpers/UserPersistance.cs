﻿namespace StudyPlanner.Helpers
{
    using System;
    using StudyPlanner.Models;

    public static class UserPersistance
    {

        private static User currentUser;

        public static User GetCurrentUser()
        {
            return currentUser;
        }

        public static void SetUser(User user)
        {
            currentUser = user;
            user.PropertyChanged += Listner;
            UserSerializer.WriteUserToXML();
        }

        public static void Listner(object sender, EventArgs e)
        {
            UserSerializer.WriteUserToXML();
        }
    }
}
