﻿namespace StudyPlanner.Helpers
{
    using System;

    public static class UserSerializer
    {
        public static void WriteUserToXML()
        {
            var user = Helpers.UserPersistance.GetCurrentUser();
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Models.User));
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//users_details.xml";
            System.IO.FileStream file = System.IO.File.Create(path);
            writer.Serialize(file, user);
            file.Close();
        }

        public static void ReadUserFromXML()
        {
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(Models.User));
            System.IO.StreamReader file = new System.IO.StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//users_details.xml");
            Models.User user = (Models.User)reader.Deserialize(file);
            file.Close();
            Helpers.UserPersistance.SetUser(user);
        }
    }
}
