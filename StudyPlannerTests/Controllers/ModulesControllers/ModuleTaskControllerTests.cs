﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static StudyPlanner.Controllers.TaskControllers.ModuleTaskController;

namespace StudyPlannerTests.Controllers.TaskControllers
{
    [TestClass]
    public class ModuleTaskControllerTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var field = typeof(User).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field != null)
                field.SetValue(null, 0);

            var field2 = typeof(StudyPlanner.Models.Module).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field2 != null)
                field2.SetValue(null, 0);

            var field3 = typeof(StudyPlanner.Models.Task).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field3 != null)
                field3.SetValue(null, 0);

            var field4 = typeof(Note).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field4 != null)
                field4.SetValue(null, 0);
        }


        [TestMethod]
        public void createModuleTaskControllerTest()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);


            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);
            UserPersistance.GetCurrentUser().GetModule(0).AddTask("task1", start, end, "description", TaskType.LECTURE);
            UserPersistance.GetCurrentUser().GetModule(0).AddTask("task2", start, end, "description", TaskType.LECTURE);
            UserPersistance.GetCurrentUser().GetModule(0).AddTask("task3", start, end, "description", TaskType.LECTURE);

            List<ReturnTasks> reTasks = ReturnAllModuleTasks(0);

            foreach (ReturnTasks task in reTasks)
            {
                if (task.TaskID == 0)
                {
                    Assert.IsTrue(task.ModuleID == 0
                        && task.TaskName == "task1");
                }
                else if (task.TaskID == 1)
                {
                    Assert.IsTrue(task.ModuleID == 0
                        && task.TaskName == "task2");
                }
                else if (task.TaskID == 2)
                {
                    Assert.IsTrue(task.ModuleID == 0
                        && task.TaskName == "task3");
                }
            }
        }
    }
}
