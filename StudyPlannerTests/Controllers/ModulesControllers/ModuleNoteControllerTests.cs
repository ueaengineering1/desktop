﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static StudyPlanner.Controllers.NoteControllers.ModuleNoteController;

namespace StudyPlannerTests.Controllers.NoteControllers
{
    [TestClass]
    public class ModuleNoteControllerTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var field = typeof(User).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field != null)
                field.SetValue(null, 0);

            var field2 = typeof(StudyPlanner.Models.Module).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field2 != null)
                field2.SetValue(null, 0);

            var field3 = typeof(StudyPlanner.Models.Task).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field3 != null)
                field3.SetValue(null, 0);

            var field4 = typeof(Note).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field4 != null)
                field4.SetValue(null, 0);
        }


        [TestMethod]
        public void createNoteControllerTests()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);

            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);
            UserPersistance.GetCurrentUser().GetModule(0).AddNote("a Note", "this is a note");
            UserPersistance.GetCurrentUser().GetModule(0).AddNote("Second Note", "this note");
            UserPersistance.GetCurrentUser().GetModule(0).AddNote("Third Note", "note");

            List<ReturnNotes> reNotes = ReturnAllModuleNotes(0);

            foreach (ReturnNotes element in reNotes)
            {
                if (element.NoteID == 0)
                {
                    Assert.IsTrue(element.ModuleID == 0
                        && element.NoteName == "a Note"
                        && element.NoteText == "this is a note");
                }
                else if (element.NoteID == 1)
                {
                    Assert.IsTrue(element.ModuleID == 0
                        && element.NoteName == "Second Note"
                        && element.NoteText == "this note");
                }
                else if (element.NoteID == 2)
                {
                    Assert.IsTrue(element.ModuleID == 0
                        && element.NoteName == "Third Note"
                        && element.NoteText == "note");
                }
            }

        }
    }
}
