﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static StudyPlanner.Controllers.ModuleControllers.ReturnModulesController;

namespace StudyPlannerTests.Controllers.ModulesControllers
{
    [TestClass]
    public class ReturnModulesControllerTests
    {

        [TestInitialize]
        public void TestInitialize()
        {
            var field = typeof(User).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field != null)
                field.SetValue(null, 0);

            var field2 = typeof(StudyPlanner.Models.Module).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field2 != null)
                field2.SetValue(null, 0);

            var field3 = typeof(StudyPlanner.Models.Task).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field3 != null)
                field3.SetValue(null, 0);

            var field4 = typeof(Note).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field4 != null)
                field4.SetValue(null, 0);
        }


        [TestMethod]
        public void createModulesControllerTests()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);

            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);
            UserPersistance.GetCurrentUser().AddModule("Graphics 1", start, end, "CMP-2004", "John Jones", 0.80f, true);

            List<ReturnModule> rm = ReturnAllModules(0);

            foreach (ReturnModule element in rm)
            {
                if (element.ModuleID == 0)
                {
                    Assert.IsTrue(element.ModuleName == "Software Engineering");
                }
                else if(element.ModuleID == 1)
                {
                    Assert.IsTrue(element.ModuleName == "Graphics 1");
                }
            }

        }
    }
}
