﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static StudyPlanner.Controllers.ModuleControllers.ReturnModuleController;

namespace StudyPlannerTests.Controllers.ModulesControllers
{
    [TestClass]
    public class ReturnModuleControllerTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var field = typeof(User).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field != null)
                field.SetValue(null, 0);

            var field2 = typeof(StudyPlanner.Models.Module).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field2 != null)
                field2.SetValue(null, 0);

            var field3 = typeof(StudyPlanner.Models.Task).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field3 != null)
                field3.SetValue(null, 0);

            var field4 = typeof(Note).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field4 != null)
                field4.SetValue(null, 0);
        }


        [TestMethod]
        public void CreateModuleControllerTests()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);

            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);
            UserPersistance.GetCurrentUser().GetModule(0).AddNote("a Note", "this is a note");
            UserPersistance.GetCurrentUser().GetModule(0).AddTask("task1", start, end, "description", TaskType.LECTURE);

            ReturnModule m = ViewModule(0);

            Assert.IsTrue(m.ModuleName == "Software Engineering"
                && m.ModuleStartDate == start
                && m.ModuleEndDate == end
                && m.ModuleCode == "CMP-05463"
                && m.ModuleOrganiser == "Peter Kay"
                && m.ModuleCourseworkAmount == 0.60f
                && m.ModuleExam == true
                && m.ModuleNotes.First().NoteName == "a Note"
                && m.ModuleNotes.First().NoteText == "this is a note"
                && m.ModuleTasks.First().TaskName == "task1");

        }
    }
}
