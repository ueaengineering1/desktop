﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static StudyPlanner.Controllers.UserControllers.CreateUserController;

namespace StudyPlannerTests.Controllers.UserController
{
    [TestClass]
    public class CreateUserControllerTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var field = typeof(User).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field != null)
                field.SetValue(null, 0);

            var field2 = typeof(StudyPlanner.Models.Module).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field2 != null)
                field2.SetValue(null, 0);

            var field3 = typeof(StudyPlanner.Models.Task).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field3 != null)
                field3.SetValue(null, 0);

            var field4 = typeof(Note).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field4 != null)
                field4.SetValue(null, 0);
        }


        [TestMethod]
        public void createUserControllerTest()
        {
            string workingDirectory = Environment.CurrentDirectory;
            string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;
            string fileName = projectDirectory + "\\users_details.xml";

            StudyPlanner.Controllers.UserControllers.CreateUserController.CreateGlobalUser("alex", "cmp", "computing", fileName);

            Assert.IsTrue(UserPersistance.GetCurrentUser().GetName() == "alex"
                && UserPersistance.GetCurrentUser().GetSchool() == "cmp"
                && UserPersistance.GetCurrentUser().GetCourse() == "computing"
                );
        }
    }
}
