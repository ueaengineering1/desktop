﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static StudyPlanner.Controllers.TaskControllers.UpdateTaskController;

namespace StudyPlannerTests.Controllers.TaskControllers
{
    [TestClass]
    public class UpateTaskControllerTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var field = typeof(User).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field != null)
                field.SetValue(null, 0);

            var field2 = typeof(StudyPlanner.Models.Module).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field2 != null)
                field2.SetValue(null, 0);

            var field3 = typeof(StudyPlanner.Models.Task).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field3 != null)
                field3.SetValue(null, 0);

            var field4 = typeof(Note).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field4 != null)
                field4.SetValue(null, 0);
        }


        [TestMethod]
        public void createUpdateTaskControllerTest()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);
            DateTime startChanged = new DateTime(2000, 10, 09); //can't change the start data to be creater than the original end date
            DateTime endChanged = new DateTime(2003, 11, 11);

            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);
            UserPersistance.GetCurrentUser().GetModule(0).AddTask("task1", start, end, "description", TaskType.LECTURE);

            UpdateTask update;
            update.ModuleId = 0;
            update.TaskId = 0;
            update.Name = "task";
            update.StartDate = startChanged;
            update.EndDate = endChanged;
            update.Description = "this has changed";
            update.Type = "LECTURE";
            update.Completion = true;

            UpdateSelectedTask(update);

            Assert.IsTrue(UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetName() == "task"
                && UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetStartDate() == startChanged
                && UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetEndDate() == endChanged
                && UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetDescription() == "this has changed"
                && UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetTaskType() == "LECTURE"
                && UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetCompletion() == true);


        }
    }
}
