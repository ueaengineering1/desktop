﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static StudyPlanner.Controllers.NoteControllers.DeleteNoteController;

namespace StudyPlannerTests.Controllers.NoteControllers
{
    [TestClass]
    public class DeleteNoteControllerTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var field = typeof(User).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field != null)
                field.SetValue(null, 0);

            var field2 = typeof(StudyPlanner.Models.Module).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field2 != null)
                field2.SetValue(null, 0);

            var field3 = typeof(StudyPlanner.Models.Task).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field3 != null)
                field3.SetValue(null, 0);

            var field4 = typeof(Note).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field4 != null)
                field4.SetValue(null, 0);
        }


        [TestMethod]
        public void createDeleteModuleNoteTest()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);

            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);
            UserPersistance.GetCurrentUser().GetModule(0).AddNote("a Note", "this is a note");

            DeleteModuleNote(0, 0);

            Assert.ThrowsException<KeyNotFoundException>(() => {
                UserPersistance.GetCurrentUser().GetModule(0).GetNote(0);
            });
        }

        [TestMethod]
        public void createDeleteTaskNoteTest()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);

            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);
            UserPersistance.GetCurrentUser().GetModule(0).AddTask("task1", start, end, "description", TaskType.LECTURE);
            UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).AddNote("a Note", "this is a note");

            DeleteTaskNote(0, 0, 0);

            Assert.ThrowsException<KeyNotFoundException>(() => {
                UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetNote(0);
             });
        }
    }
}
