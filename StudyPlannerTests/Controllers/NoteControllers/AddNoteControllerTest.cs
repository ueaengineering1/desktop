﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudyPlanner.Controllers.NoteControllers;
using System.Reflection;

namespace StudyPlannerTests.Controllers.NoteControllers
{
    [TestClass]
    public class AddNoteControllerTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var field = typeof(User).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field != null)
                field.SetValue(null, 0);

            var field2 = typeof(StudyPlanner.Models.Module).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field2 != null)
                field2.SetValue(null, 0);

            var field3 = typeof(StudyPlanner.Models.Task).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field3 != null)
                field3.SetValue(null, 0);

            var field4 = typeof(Note).GetField("idIncrement", BindingFlags.Static | BindingFlags.NonPublic);
            if (field4 != null)
                field4.SetValue(null, 0);
        }

        [TestMethod]
        public void createModuleNoteControllerTest()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);

            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);

            AddNoteController.AddModuleNote(UserPersistance.GetCurrentUser().GetModule(0).GetId(),"Note","This is a note");

            Assert.IsTrue(UserPersistance.GetCurrentUser().GetModule(0).GetNote(0).GetName() == "Note"
                            && UserPersistance.GetCurrentUser().GetModule(0).GetNote(0).GetText() == "This is a note");
        }

        [TestMethod]
        public void createTaskNoteControllerTest()
        {

            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);

            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true);
            UserPersistance.GetCurrentUser().GetModule(0).AddTask("task1", start, end, "description", TaskType.LECTURE);

            AddNoteController.AddTaskNote(0, 0, "Task Note", "This is a task Note");

            Assert.IsTrue(UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetNote(0).GetName() == "Task Note"
                            && UserPersistance.GetCurrentUser().GetModule(0).GetTask(0).GetNote(0).GetText() == "This is a task Note");

        }
    }
}
