﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlannerTests.Models
{
    [TestClass()]
    public class UserTests
    {
        [TestMethod()]
        public void createUserTest()
        {
            User user = User.CreateUser("Alex Taberner", "CMP", "Computing Science");
            Assert.IsTrue(user.GetName() == "Alex Taberner"
                && user.GetSchool() == "CMP"
                && user.GetCourse() == "Computing Science");
        }
    }
}