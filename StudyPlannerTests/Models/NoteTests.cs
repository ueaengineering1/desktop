﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlannerTests.Models
{
    [TestClass]
    public class NoteTests
    {
        [TestMethod]
        public void createNoteTest()
        {
            Note test = Note.CreateNote("a Note", "some text");

            Assert.IsTrue(
                test.GetText() == "some text"
                && test.GetName() == "a Note"
            );
        }
        
    }
}
