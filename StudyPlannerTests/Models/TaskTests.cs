﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudyPlannerTests.Models
{
    [TestClass]
    public class TaskTests
    {
        [TestMethod]
        public void CreateTaskTest()
        {
            DateTime start = DateTime.Now, end = DateTime.Now;

            Task test = Task.CreateTask("task1", start, end, "description", TaskType.LECTURE);

            Assert.IsTrue(test.GetName() == "task1"
                && test.GetStartDate() == start
                && test.GetEndDate() == end
                && test.GetDescription() == "description"
                && test.GetTaskTypeObj() == TaskType.LECTURE);
        }
    }
}
