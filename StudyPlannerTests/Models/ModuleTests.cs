﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlannerTests.Models
{
    [TestClass]
    public class ModuleTests
    {
        [TestMethod]
        public void createModuleTest()
        {
            DateTime start = new DateTime(2000, 11, 11);
            DateTime end = new DateTime(2001, 11, 11);

            Module module = Module.CreateModule("Software Engineering", start, end, "CMP-05463", "Peter Kay", 0.60f, true); 
            Assert.IsTrue(module.GetName() == "Software Engineering"
                && module.GetStartDate() == start
                && module.GetEndDate() == end
                && module.GetModuleCode() == "CMP-05463"
                && module.GetModuleOrganiser() == "Peter Kay"
                && module.GetCourseworkAmount() == 0.60f
                && module.GetExam() == true);
        }
    }
}