﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlannerTests.Helpers
{
    [TestClass]
    public class UserSerializerTests
    {
        [TestMethod]
        public void TestUserProfileExportImport()
        {
            DateTime s = DateTime.Now, e = DateTime.Now;

            User u = User.CreateUser("alex", "cmp", "yay");

            UserPersistance.SetUser(u);

            UserPersistance.GetCurrentUser().AddModule("name", s, e, "code", "org", 30.0f, true);
            UserPersistance.GetCurrentUser().GetModule(1).AddTask("tname", s, e, "desc", TaskType.LECTURE);
            UserPersistance.GetCurrentUser().GetModule(1).AddNote("namen", "Some text");

            UserSerializer.ReadUserFromXML();

            Assert.IsTrue(
                UserPersistance.GetCurrentUser().GetName() == "alex"
                && UserPersistance.GetCurrentUser().GetModule(1).GetName() == "name"
                && UserPersistance.GetCurrentUser().GetModule(1).GetTask(1).GetName() == "tname"
            );
        }
    }
}
