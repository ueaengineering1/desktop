﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudyPlanner.Helpers;
using StudyPlanner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudyPlannerTests.Helpers
{
    [TestClass]
    public class UserPersistenceTests
    {
        [TestMethod]
        public void CreateUserPersistenceTest()
        {
            User u = User.CreateUser("alex", "cmp", "computing");
            UserPersistance.SetUser(u);

            Assert.IsTrue(UserPersistance.GetCurrentUser().GetName() == "alex"
                && UserPersistance.GetCurrentUser().GetSchool() == "cmp"
                && UserPersistance.GetCurrentUser().GetCourse() == "computing");
        }
    }
}
